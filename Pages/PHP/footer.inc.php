
<footer>
 <div class="row">

  <div class="col-sm">
   <nav>
    <h3>About Us</h3>
    <ul>
        <li><a href="AboutUs.php#WebSite">The Website</a></li>
        <li><a href="AboutUs.php#Members">Members</a></li>
    </ul>
   </nav>
  </div>

  <div class="col-sm">
    <nav>
     <h3>Contact Us</h3>
     <ul>
         <li><a href="AboutUs.php#Emails">Emails</a></li>
         <li><a href="AboutUs.php#Tel">Tel</a></li>
         <li><a href="AboutUs.php#SocialNetworks">Social Networks</a></li>
     </ul>
    </nav>
  </div>

</div>

</footer>

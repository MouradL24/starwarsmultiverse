<!--Descriptif des personnages-->
<?php
//Title_page
$title_page="Catalog";

//StyleSheet
$style_file="../CSS/FullContent.css";

//Head
include("head.inc.php");
include("film_funtion.inc.php");
?>


<body>
<?php
    //Header
    include("header.inc.php");

    //Check Log
    if(empty($_SESSION["logged"]))
    header('Location:Login.php');   
    ?>

<main>
<div class="container-fluid"> 
    
        <div class="row">
        
            <div  class="col-sm-3">
                <br/>
                <br/>
                <?php 
                $id=$_GET['id'];
                //$portrait=get_Picture_People($id);
                if(!empty($poster_people[$id])) $portrait=$poster_people[$id]?>

                <img src=<?php if(!empty($portrait)) echo $portrait; ?> alt="Portrait" style="width: 100%">
            </div>

           
            <div  class="col-sm-9">
                <div class="info">
                    <?php 
                    $perso=get_People_info($id);
                    if(!empty($perso)){
                        $name=$perso["name"];
                        $gender=$perso["gender"];
                        $height=$perso["height"];
                        $masse=$perso["mass"];
                        $idhomeworld=$perso["homeworld"];
                    }
                    else{
                        $name="";
                        $gender="";
                        $height="";
                        $masse="";
                        $homeworld="";

                    }
                    ?>
                    <br/><br/>
                    <h2><?php echo $name?></h2>
                    <br/>
                    <p><?php echo  "Gender :".$gender?></p>
                    <p><?php echo "Height :" .$height?></p>
                    <p><?php echo "Masse :" .$masse?></p>
                </div>

            
                <div class="Planet">
                    <p><?php
                    $planet=get_homeworld($idhomeworld);
                    if(!empty($planet)){
                        $nameP=$planet["name"];   
                    }
                    else{
                        $nameP="";
                    }
                    ?> <br/>
                    <p><?php echo "Planet: " .$nameP ?>
                    </p>
                </div>
            </div>    
        </div>                 
    </div>
    </main>
    <br/><br/>

    <?php 
    //Footer
    include("footer.inc.php");
    ?>

    <script src="js/bootstrap.min.js"></script>;


</body>
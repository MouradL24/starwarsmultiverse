<?php

//Get Film Id with Episode
function get_id_Film($episode){
    
    $mysqli=Connection();
    if(!empty($mysqli)){
        if (!($stmt = $mysqli->prepare("SELECT `id`FROM `film` WHERE `episode` = ?"))) 
        {
            echo "Echec de la préparation : (" . $mysqli->errno . ") " . $mysqli->error;
            return false;
        }
            $stmt->bind_param('i', $episode);

        if (!$stmt->execute()) {
            echo "Echec lors de l’exécution de la requête : (" . $stmt->errno . ") " . $stmt->error;
            return false;
        }
        
        else{
            $res=$stmt->get_result();

            if($res->num_rows == 0){
                return false ;
                echo "Aucune ligne trouvee";
            }   
            else{
            $donnees=$res->fetch_assoc();   
            return $donnees["id"];           
         }
        }
    }
}

// Get information for the page Full content
function get_information_Films($id){
    $mysqli=Connection();
    if(!empty($mysqli)){
        if (!($stmt = $mysqli->prepare("SELECT `title`, `episode`, `release_date`, `opening` FROM `film` WHERE `id`=?")))
        {
            echo "Echec de la préparation : (" . $mysqli->errno . ") " . $mysqli->error;
            return false;
        }

        $stmt->bind_param('i', $id);

        if (!$stmt->execute()) {
            echo "Echec lors de l’exécution de la requête : (" . $stmt->errno . ") " . $stmt->error;
            return false;
        }
    
        else{
            $res=$stmt->get_result();
            if($res->num_rows == 0){
            return false ;
             }   
            else{
            $row=$res->fetch_assoc();
            return $row;   
            }
        }
    }
}

function get_Film_Actor($id){
    $mysqli=Connection();
    if(!empty($mysqli)){
        if (!($stmt = $mysqli->prepare("SELECT `name` FROM `people` INNER JOIN `playsin` ON (people.id=playsin.id_people) WHERE `id_film`=?")))
        {
            echo "Echec de la préparation : (" . $mysqli->errno . ") " . $mysqli->error;
            return false;
        }

        $stmt->bind_param('i', $id);

        if (!$stmt->execute()) {
            echo "Echec lors de l’exécution de la requête : (" . $stmt->errno . ") " . $stmt->error;
            return false;
        }     
        else
        {
            $res=$stmt->get_result();
            if($res->num_rows == 0)
            {
                return false ;
            }   
            else
            {    $i=0;
                while($row=$res->fetch_assoc()){
                    $people[$i]=$row;
                   // echo $people[$i]["name"];
                    $i=$i+1;
                    
                }
            return $people;  
            }
        }
    }
}

?>
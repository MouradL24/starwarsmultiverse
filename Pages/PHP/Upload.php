<?php
//Title_page
$title_page="Upload";

//StyleSheet
$style_file="../CSS/Upload_Style.css";

//Head
include("head.inc.php");
?>

<body>

<?php
//Check Log
if(empty($_SESSION["logged"]) || $_SESSION["role"]<3)
header('Location:Home.php');
?>

<main>

<?php
     
     if(!empty($_SESSION["error_upload"])){
       if(isset($_GET['error'])){
        if($_GET['error']==0)
        echo '<span class="alert alert-success"><strong>Sent !</strong></span>';
         elseif($_GET['error']==1)
       echo '<span class="alert alert-warning"><strong>Unauthorized Extension!</strong></span>';
       elseif($_GET['error']==2)
       echo '<span class="alert alert-warning"><strong>File too Big!</strong></span>';
        else
        echo '<span class="alert alert-warning"><strong>Error!</strong></span>';     
       }

       echo '<br>';
       echo '<br>';

       unset($_SESSION["error_upload"]);
      }
            ?>

            

        <form action="action_Upload.php" method="post" enctype="multipart/form-data">

<div class="form-group">
<label for="pictures">File : </label><br />
<input type="file" name="pictures" />
</div>


<div class="form-group">
        <label for="category">Type : </label><br />
       <select name="category" id="category">
        <option value="Films">Films</option>
        <option value="People">People</option>
        <option value="Planets">Planets</option>
        <option value="Species">Species</option>
        <option value="Starships">Starships</option>
        <option value="Starships">News</option>
        <option value="Starships">Backgrounds</option>
        <option value="Starships">Logos</option>
       </select>
       </div>

<br>
<button type="submit" class="btn btn-primary" name="button" value="Upload">Upload</button>
</form>

<br>
<a href="Settings.php">Back</a>
</main>


</body>
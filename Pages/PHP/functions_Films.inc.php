<?php

/***************************************************FILMS*********************************************************************/

//get Film
function get_Film($episode){

    $mysqli=Connection();
    if(!empty($mysqli)){
    if (!($stmt = $mysqli->prepare("SELECT * FROM film WHERE episode = ? "))) 
    {
    echo "Echec de la préparation : (" . $mysqli->errno . ") " . $mysqli->error;
    return false;
    }

    $stmt->bind_param('i', $episode);

    if (!$stmt->execute()) {
        echo "Echec lors de l’exécution de la requête : (" . $stmt->errno . ") " . $stmt->error;
        return false;
        }
    
    else{
        $res=$stmt->get_result();

    if($res->num_rows == 0){
         return false ;
    }   
    else{
    $row=$res->fetch_assoc();   
    return $row;
    }
}
}

else
return false;
}

//get all Films
function get_all_Films(){
    $mysqli=Connection();
    if(!empty($mysqli)){
    if (!($stmt = $mysqli->prepare("SELECT * FROM film  ORDER BY `episode` "))) 
    {
    echo "Echec de la préparation : (" . $mysqli->errno . ") " . $mysqli->error;
    return false;
    }

    if (!$stmt->execute()) {
        echo "Echec lors de l’exécution de la requête : (" . $stmt->errno . ") " . $stmt->error;
        return false;
        }
    
    else{
        $res=$stmt->get_result();

    if($res->num_rows == 0){
         return false ;
    }   
    else{
        $i=0;
    while($row=$res->fetch_assoc()){
        $film[$i]=$row;
        $i=$i+1;
    }  
    return $film;
    }
}
}
else
return false;
}

//insert Film
function insert_Film($title,$release_date,$episode,$opening){
   
    $mysqli=Connection();
    if(!empty($mysqli)){
        if (!($stmt = $mysqli->prepare("INSERT INTO `film` (`id`, `title`, `release_date`, `episode`, `opening`) VALUES (NULL,?, ?,?, ?)")))  {
    echo "Echec de la préparation : (" . $mysqli->errno . ") " . $mysqli->error;
    }
    else{
    
    $release_dateSQL = date('Y-m-d',strtotime($release_date));
    $stmt->bind_param('ssis',$title,$release_dateSQL,$episode,$opening);

    if (!$stmt->execute()) {
        echo "Echec lors de l’exécution de la requête : (" . $stmt->errno . ") " . $stmt->error;
        return false;
        }
    else{
      return true;  
    }

    }

    }

}


//Edit Film
function edit_Film($title,$release_date,$episode,$opening){


    $film=get_Film($episode);
    $id_film=$film["id"];

    $mysqli=Connection();
    if(!empty($mysqli)){
        if (!($stmt = $mysqli->prepare("UPDATE `film` SET  `title`=?, `release_date`=?, `episode`=?, `opening`=? WHERE `id`=?")))  {
    echo "Echec de la préparation : (" . $mysqli->errno . ") " . $mysqli->error;
    }
    else{
    
    $release_dateSQL = date('Y-m-d',strtotime($release_date));
    $stmt->bind_param('ssisi',$title,$release_dateSQL,$episode,$opening,$id_film);

    if (!$stmt->execute()) {
        echo "Echec lors de l’exécution de la requête : (" . $stmt->errno . ") " . $stmt->error;
        return false;
        }
    else{
      return true;  
    }

    }

    }

}

//delete Film
function delete_Film($episode){
    $mysqli=Connection();
    if(!empty($mysqli)){
    if (!($stmt = $mysqli->prepare("DELETE FROM `film` WHERE `film`.`episode` = ?"))) 
    {
    echo "Echec de la préparation : (" . $mysqli->errno . ") " . $mysqli->error;
    return false;
    }

    $stmt->bind_param('i',$episode);


    if (!$stmt->execute()) {
        echo "Echec lors de l’exécution de la requête : (" . $stmt->errno . ") " . $stmt->error;
        return false;
        }
    
    else{
      return true;  
}
}

else
return false;
}


//Get Poster Film
function get_Poster_Film($episode){
    $mysqli=Connection();
    if(!empty($mysqli)){
    if (!($stmt = $mysqli->prepare("SELECT * FROM pictures INNER JOIN film ON (pictures.id=film.id)  WHERE `episode`=?"))) 
    {
    echo "Echec de la préparation : (" . $mysqli->errno . ") " . $mysqli->error;
    return false;
    }

    $stmt->bind_param('i',$episode);


    if (!$stmt->execute()) {
        echo "Echec lors de l’exécution de la requête : (" . $stmt->errno . ") " . $stmt->error;
        return false;
        }
    
    else{
        $res=$stmt->get_result();

    if($res->num_rows == 0){
         return false ;
    }   
    else{
    $row=$res->fetch_assoc();
    return $row;
    }
}
}
else
return false;
}

function get_all_Posters_Films(){
    $mysqli=Connection();
    if(!empty($mysqli)){
    if (!($stmt = $mysqli->prepare("SELECT * FROM pictures INNER JOIN film ON (pictures.id=film.id)"))) 
    {
    echo "Echec de la préparation : (" . $mysqli->errno . ") " . $mysqli->error;
    return false;
    }

    if (!$stmt->execute()) {
        echo "Echec lors de l’exécution de la requête : (" . $stmt->errno . ") " . $stmt->error;
        return false;
        }
    
    else{
        $res=$stmt->get_result();

    if($res->num_rows == 0){
         return false ;
    }   
    else{
    while($row=$res->fetch_assoc()){
        $i=$row["id"];
        $poster_film[$i]=$row["path"];
    }  
    return $poster_film;
    }
}
}
else
return false;
}

//Change Poster
function change_Poster_Film($episode,$path){
  
    $film=get_Film($episode);
    $id_film=$film["id"];

    $row=get_Poster_Film($episode); 

    if(empty($row)){  
     $mysqli=Connection();
     if(!empty($mysqli)){
         if (!($stmt = $mysqli->prepare("INSERT INTO `pictures` (`id_picture`, `id`,`path`) VALUES (NULL, ?, ?)")))  {
     echo "Echec de la préparation : (" . $mysqli->errno . ") " . $mysqli->error;
     }
     else{
     $stmt->bind_param('is',$id_film,$path);
 
     if (!$stmt->execute()) {
         echo "Echec lors de l’exécution de la requête : (" . $stmt->errno . ") " . $stmt->error;
         return false;
         }
     else{
       return true;  
     }
 
     }
 
     }
     else
     return false;
 }

 else{
    $mysqli=Connection();
    if(!empty($mysqli)){
    if (!($stmt = $mysqli->prepare("UPDATE pictures SET `path` = ?  WHERE  `id`= ?"))) 
    {
    echo "Echec de la préparation : (" . $mysqli->errno . ") " . $mysqli->error;
    return false;
    }

    $stmt->bind_param('si',$path,$id_film);


    if (!$stmt->execute()) {
        echo "Echec lors de l’exécution de la requête : (" . $stmt->errno . ") " . $stmt->error;
        return false;
        }
    
    else{
      return true;  
}
}

else
return false;
} 
}



?>
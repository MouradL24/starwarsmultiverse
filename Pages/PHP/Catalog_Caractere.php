<!--Page de listing de tous les personnages-->


<?php
//Title_page
$title_page="Character";

//StyleSheet
$style_file="../CSS/CatalogStyle.css";

//Head
include("head.inc.php");
?>


<body>
    <?php
    //Check Log
    if(empty($_SESSION["logged"]))
    header('Location:Login.php');

    //Header
    include("header.inc.php");
    ?>



<br/>
<div  style="text-align : center">
<!--Error-->
<?php
     
     if(!empty($_SESSION["error_edit"])){
       if(isset($_GET['error'])){
        if($_GET['error']==0)
        echo '<span class="alert alert-success"><strong>Success !</strong></span>';
         elseif($_GET['error']==1)
       echo '<span class="alert alert-warning"><strong>Error!</strong></span>';
       elseif($_GET['error']==2)
       echo '<span class="alert alert-warning"><strong>Error !</strong></span>';
        else
        echo '<span class="alert alert-warning"><strong>Error!</strong></span>';     
       }

       unset($_SESSION["error_edit"]);
      }

      if(!empty($_SESSION["error_delete"])){
        if(isset($_GET['error'])){
         if($_GET['error']==0)
         echo '<span class="alert alert-success"><strong>Success !</strong></span>';
          elseif($_GET['error']==1)
        echo '<span class="alert alert-warning"><strong>Error!</strong></span>';
        elseif($_GET['error']==2)
        echo '<span class="alert alert-warning"><strong>Error !</strong></span>';
         else
         echo '<span class="alert alert-warning"><strong>Error!</strong></span>';     
        }
 
        unset($_SESSION["error_delete"]);
       }
            ?>

      </div>
      <br/>

      <?php if($_SESSION["role"] == 3) { ?>
<form action="Add_People.php"  style="text-align : center">
<div>
        <button type="submit" class="btn btn-success" name="button" value="People">Add More People</button>
</div>  
</form>
<?php
}
?>
    <br/>

<main>

    <div class="grid-container"> 

    <?php

        $max=sizeof($people_glob);

        for($i=0;$i<$max;$i++){
            $people_name=$people_glob[$i]["name"];
            //$people_name=htmlentities($people_name);
            //$episode=$film[$i]["episode"];
            $id=$people_glob[$i]["id"];
            if(!empty($poster_people[$id])) $poster=$poster_people[$id]; else $poster="";
    ?>

        <div  class="grid-item">

            <a href="FullContent_Charactere.php?id=<?php echo $id;?>" >
            <h3  style="text-align : center"><?php echo $people_name;?></h3>
            <img src=<?php if(!empty($poster)) echo $poster; ?> class="Affiche" alt="Affiche" >
            </a>
        
        <br/>
            <?php if($_SESSION["role"] == 3) { ?>
                
            <form action="Edit_People.php" method="POST"  style="text-align : center">
                    <button type="submit" class="btn btn-primary" name="button" value="<?php echo $people_name;?>">Edit</button> 
            </form>

            <form action="action_DeleteCatalog.php?category=People" method="POST"  style="text-align : center">
                    <button type="submit" class="btn btn-danger" name="button" value="<?php echo $people_name;?>">Delete</button> 
            </form>
            
            <?php
            }
            ?>
      

        </div>

      <?php
        }
      
      ?>
</div>

    </main>

<br/>
<?php /*

</br>
<h1>First Trilogy</h1>
    </br>

    <div class="container"> 
        <div class="row">

            <!-- Déclaration de colonne dynamique--> 
            <div  class="col-md-4">
            <!-- Envoi l'info de l'id d'une page à l'autre -->
            <a href="FullContent_Charactere.php?id=1">Luke Skywalker 
            <!-- Récupération de l'image dans depuis la BDD et affichage --> 
            <img src=<?php 
            $picture=get_Picture_People(1);
            if(!empty($picture)) echo $picture ?> id="AfficheI" alt="AfficheI" style="width: 100%">            
            </div></a>

            <div  class="col-md-4">
           <a href="FullContent_Charactere.php?id=2"> C-3PO
            <img src=<?php 
            $picture=get_Picture_People(2);
            if(!empty($picture)) echo $picture ?> id="AfficheI" alt="AfficheI" style="width: 100%"> 
            </div></a>

            <div  class="col-md-4">
            <a href="FullContent_Charactere.php?id=3">R2-D2
            <img src=<?php 
            $picture=get_Picture_People(3);
            if(!empty($picture)) echo $picture ?> id="AfficheI" alt="AfficheI" style="width: 100%"> 
            </div></a>

        </div>
    </div>  
    


</br></br>

<div class="container"> 
    <div class="row">

        <div  class="col-md-4">
        <a href="FullContent_Charactere.php?id=4">Darth Vader
        <img src=<?php 
        $picture=get_Picture_People(4);
        if(!empty($picture)) echo $picture ?> id="AfficheI" alt="AfficheI" style="width: 100%"> 
        </div></a>

        <div  class="col-md-4">
        <a href="FullContent_Charactere.php?id=5">Leia Organa
        <img src=<?php 
        $picture=get_Picture_People(5);
        if(!empty($picture)) echo $picture ?> id="AfficheI" alt="AfficheI" style="width: 100%"> 
        </div></a>

        <div  class="col-md-4">
        <a href="FullContent_Charactere.php?id=6">Owen Lars
        <img src=<?php
        $picture=get_Picture_People(6);
        if(!empty($picture)) echo $picture?> id="AfficheI" alt="AfficheI" style="width: 100%"> 
        </div></a>
    </div>
</div>  

</br></br>

<div class="container"> 
    <div class="row">

        <div  class="col-md-4">
        <a href="FullContent_Charactere.php?id=7">Obi-Wan Kenobi
        <img src=<?php 
        $picture=get_Picture_People(7);
        if(!empty($picture)) echo $picture ?> id="AfficheI" alt="AfficheI" style="width: 100%"> 
        </div></a>

        <div  class="col-md-4">
        <a href="FullContent_Charactere.php?id=8">Anakin Skywalker
        <img src=<?php
        $picture=get_Picture_People(8);
        if(!empty($picture)) echo $picture ?> id="AfficheI" alt="AfficheI" style="width: 100%"> 
        </div></a>

        <div  class="col-md-4">
        <a href="FullContent_Charactere.php?id=9">Chewbacca
        <img src=<?php 
        $picture=get_Picture_People(9);
        if(!empty($picture)) echo $picture ?> id="AfficheI" alt="AfficheI" style="width: 100%"> 
        </div></a>

    </div>
</div> 

</br></br>

<div class="container"> 
    <div class="row">

        <div  class="col-md-4">
        <a href="FullContent_Charactere.php?id=10">Han Solo
        <img src=<?php 
        $picture=get_Picture_People(10);
        if(!empty($picture)) echo $picture ?> id="AfficheI" alt="AfficheI" style="width: 100%"> 
        </div></a>
    </div>
</div>  

*/ ?>
    <?php 
    include("footer.inc.php");
    ?>


</body>
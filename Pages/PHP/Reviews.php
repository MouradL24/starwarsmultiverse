<?php
//Title_page
$title_page="Reviews";

//StyleSheet
$style_file="../CSS/Reviews_Style.css";

//Head
include("head.inc.php");
?>

<body>

<?php
//Check Log
if(empty($_SESSION["logged"]))
header('Location:Login.php');

//Header
include("header.inc.php");
?>

<main>

    <section>
      <div class="row">
        <div class="col-sm-8">
    <form action="action_Reviews.php" method="POST">
    <?php

    //Get film
    if(!empty($_GET["film"]))
    $film=get_Film($_GET["film"]);

    //Post
    if(!empty($film)){
    echo '<input type="hidden" id="film" name="film" value='.$film["id"].'>';
    echo '<input type="hidden" id="episode" name="episode" value='.$film["episode"].'>';
    $film_title=$film["title"];
    }
    else
    $film_title="Movie name"
    ?>
   <h1>What do you think about <strong><?php echo $film_title?></strong> ? :</h1>
<div class="form-check">
  <input class="form-check-input" type="radio" name="reviews" id="reviews1" value="Perfect">
  <label class="form-check-label" for="reviews1">
    Perfect!
  </label>
</div>

<div class="form-check">
  <input class="form-check-input" type="radio" name="reviews" id="reviews2" value="Excellent">
  <label class="form-check-label" for="reviews2">
    Excellent
  </label>
</div>

<div class="form-check">
  <input class="form-check-input" type="radio" name="reviews" id="reviews3" value="Very Good">
  <label class="form-check-label" for="reviews3">
    Very Good
  </label>
</div>

<div class="form-check">
  <input class="form-check-input" type="radio" name="reviews" id="reviews4" value="Good">
  <label class="form-check-label" for="reviews4">
    Good
  </label>
</div>

<div class="form-check">
  <input class="form-check-input" type="radio" name="reviews" id="reviews5" value="Not Bad">
  <label class="form-check-label" for="reviews5">
    Not bad
  </label>
</div>

<div class="form-check">
  <input class="form-check-input" type="radio" name="reviews" id="reviews6" value="Poor">
  <label class="form-check-label" for="reviews6">
    Poor
  </label>
</div>

<div class="form-check">
  <input class="form-check-input" type="radio" name="reviews" id="reviews7" value="Very Bad">
  <label class="form-check-label" for="reviews7">
    Very Bad
  </label>
</div>

<div class="form-check">
  <input class="form-check-input" type="radio" name="reviews" id="reviews8" value="Not Seen" checked>
  <label class="form-check-label" for="reviews8">
    Not Seen
  </label>
</div>

<br>
<div class="form-group">
  <label for="comment" id="mycomment">Comment:</label>
  <textarea class="form-control" name="comment" id="comment" rows="10" cols="80"></textarea>
</div>


<br>
<button type="submit" class="btn btn-primary" name="button">Confirm</button>
    </form>

</div>

<div class="col-sm-4">
<img src=<?php if(!empty($poster_film[$film["id"]])) echo $poster_film[$film["id"]]; ?> id="Poster" alt="Poster">
</div>
</div>
</section>

<?php
$count_Perfect=get_Count_Reviews($film["id"],"Perfect");
$count_Excellent=get_Count_Reviews($film["id"],"Excellent");
$count_VeryGood=get_Count_Reviews($film["id"],"Very Good");
$count_Good=get_Count_Reviews($film["id"],"Good");
$count_NotBad=get_Count_Reviews($film["id"],"Not Bad");
$count_Poor=get_Count_Reviews($film["id"],"Poor");
$count_VeryBad=get_Count_Reviews($film["id"],"Very Bad");
$count_NotSeen=get_Count_Reviews($film["id"],"Not Seen");
$count_Reviews_Film=get_Count_Reviews_Film($film["id"]);


if(!empty($count_Reviews_Film["count_reviews"])){
if(!empty($count_Perfect)) $Perfect=100*$count_Perfect["count_reviews"]/$count_Reviews_Film["count_reviews"];
if(!empty($count_Excellent)) $Excellent=100*$count_Excellent["count_reviews"]/$count_Reviews_Film["count_reviews"];
if(!empty($count_VeryGood)) $VeryGood=100*$count_VeryGood["count_reviews"]/$count_Reviews_Film["count_reviews"];
if(!empty($count_Good)) $Good=100*$count_Good["count_reviews"]/$count_Reviews_Film["count_reviews"];
if(!empty($count_NotBad)) $NotBad=100*$count_NotBad["count_reviews"]/$count_Reviews_Film["count_reviews"];
if(!empty($count_Poor)) $Poor=100*$count_Poor["count_reviews"]/$count_Reviews_Film["count_reviews"];
if(!empty($count_VeryBad)) $VeryBad=100*$count_VeryBad["count_reviews"]/$count_Reviews_Film["count_reviews"];
if(!empty($count_NotSeen)) $NotSeen=100*$count_NotSeen["count_reviews"]/$count_Reviews_Film["count_reviews"];
}

else{
  $Perfect=0;
  $Excellent=0;
  $VeryGood=0;
  $Good=0;
  $NotBad=0;
  $Poor=0;
  $VeryBad=0;
  $NotSeen=0;

}

?>

<section>
   <h1>General Opinion:</h1>

<div class="row">

<div class="col-sm">
   <div class="row">
 <label>Perfect : </label>
 <p><?php if(isset($Perfect)) echo $Perfect.'%'?></p>
</div>


<div class="row">
<label>Excellent : </label>
 <p><?php if(isset($Excellent)) echo $Excellent.'%'?></p>
</div>

<div class="row">
<label>Very Good : </label>
 <p><?php if(isset($VeryGood)) echo $VeryGood.'%'?></p>
</div>

<div class="row">
<label>Good : </label>
 <p><?php if(isset($Good)) echo $Good.'%'?></p>
</div>
</div>

<div class="col-sm">
<div class="row">
<label>Not Bad : </label>
 <p><?php if(isset($NotBad)) echo $NotBad.'%'?></p>
</div>

<div class="row">
<label>Poor : </label>
 <p><?php if(isset($Poor)) echo $Poor.'%'?></p>
</div>

<div class="row">
 <label>Very Bad : </label>
 <p><?php if(isset($VeryBad)) echo $VeryBad.'%'?></p>
</div>

<div class="row">
<label>Not Seen : </label>
 <p><?php if(isset($NotSeen)) echo $NotSeen.'%'?></p>
</div>
</div>

</div>
</section>


<?php
  $review=get_Reviews($_SESSION["id_user"],$film["id"]);
  if(!empty($review)){
  $date=$review["date"];
  $opinion=$review["review"];
  $comment=$review["comment"];
  }
  else{
    $date="";
    $opinion="";
    $comment="(Empty)";
  }
?>
<section>
  <h1>My Comment :</h1>
  <div class="comment">
  <h2><strong>Me</strong> : <em><?php echo $date?></em></h2>
  <h3><?php echo $opinion?></h3>
  <p><?php echo $comment?></p>
  </div>
</section>

<section>
  <h1>Users comments :</h1>

<?php
$reviews=get_all_Reviews($film["id"]);

if(!empty($reviews)){
  $max=sizeof($reviews);
  for($i=0;$i<$max;$i++){
    $username=$reviews[$i]["username"];
    $date=$reviews[$i]["date"];
    $opinion=$reviews[$i]["review"];
    $comment=$reviews[$i]["comment"];
    ?>
  <div class="comment">
  <h2><strong><?php echo $username?></strong> : <em><?php echo $date?></em></h2>
  <h3><?php echo $opinion?></h3>
  <p><?php echo $comment?></p>
  </div>

    <?php

  }

}

else{
?>
  <div class="comment">
  <p>(Empty)</p>
  </div>
<?php
}
?>

</section>
</main>


<?php 
//Footer
include("footer.inc.php");
?>

</body>